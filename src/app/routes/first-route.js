module.exports = (app) => {

    app.get('/', (req, res) => {
        res.send(
            `
            <html>
                <head>
                    <meta charset="utf-8">
                </head>
                <body>
                    <h2>Code House :)</h2>
                </body>
            </html>
            `
        );
    });

    app.get('/books', (req, res) => {
        res.marko(
            require('../views/books/list/list.marko'),
            {
                books: [
                    {
                        id: 1,
                        title: 'Fundamentos do Node'
                    },
                    {
                        id: 2,
                        title: 'Node avançado'
                    }
                ]
            }
        );
    });

}